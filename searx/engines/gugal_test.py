# SPDX-License-Identifier: AGPL-3.0-or-later
"""Within this module we implement a *demo offline engine*.  Do not look to
close to the implementation, its just a simple example.  To get in use of this
*demo* engine add the following entry to your engines list in ``settings.yml``:

.. code:: yaml

  - name: test engine
    engine: gugal_test
    shortcut: test
    disabled: false

"""

import json
from datetime import datetime

engine_type = 'offline'
categories = ['general']
disabled = True
timeout = 2.0

about = {
    "wikidata_id": None,
    "official_api_documentation": None,
    "use_official_api": False,
    "require_api_key": False,
    "results": 'JSON',
}


def init(engine_settings=None):
    # this engine doesn't need init
    print("init")

def search(query, request_params):
    # if a url test was requested, respond with a ton of urls with different schemes
    if query == "urltest":
        return [
            {
                'url': "https://example.com/secure",
                'title': "HTTPS",
                'content': "https://example.com/secure (rend_as=example.com)",
                "publishedDate": datetime.now()
            },
            {
                'url': "http://example.com/insecure",
                'title': "HTTP",
                'content': "http://example.com/insecure (rend_as=example.com)",
                "publishedDate": datetime.now()
            },
            {
                'url': "ftp://ftpserver.example.com",
                'title': "FTP",
                'content': "ftp://ftpserver.example.com (rend_as=ftpserver.example.com)",
                "publishedDate": datetime.now()
            },
            {
                'url': "sftp://sftpserver.example.com",
                'title': "SFTP",
                'content': "sftp://sftpserver.example.com (rend_as=sftpserver.example.com)",
                "publishedDate": datetime.now()
            },
            {
                'url': "http://username:password@example.com",
                'title': "HTTP w/ auth",
                'content': "http://username:password@example.com (rend_as=username:password@example.com)",
                "publishedDate": datetime.now()
            }
        ]

    # add 10 results
    ret_val = []
    for i in range(10):
        entry = {
            'url': "https://gitlab.com/gugal/testxng?gte_param="+str(i),
            'title': "Test result " + str(i),
            'content': "result #" + str(i) + " for q=" + query + " (generated using test engine gugal_test.py)",
            "publishedDate": datetime.now()
        }
        ret_val.append(entry)

    return ret_val
